import re


class Entry(object):
    head = ''
    body = ''

    def __repr__(self):
        return 'head: {:s} \nbody: {:s}'.format(self.head, self.body)


def analyse(file_name, head_re):
    head_re = re.compile(head_re)
    entry = None
    body = None
    with open(file_name) as f:
        for line in f:
            if re.match(head_re, line) is not None:  # new entry
                if entry is not None:
                    entry.body = ''.join(body)
                    yield entry
                entry = Entry()
                entry.head = line
                body = []
            else:
                body.append(line)
        if entry is not None:
            entry.body = ''.join(body)
            yield entry
