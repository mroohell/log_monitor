#!/usr/bin/env python2.7
# encoding=utf8
from glob import glob
from datetime import datetime
import os

from log_monitor.analyst import analyse
from log_monitor.postman import send
from log_monitor import config as CONFIG


def main():
    error_report = []
    for log_file in glob(CONFIG.LOGS_GLOB):
        if datetime.fromtimestamp(os.path.getmtime(log_file)).day != datetime.now().day:
            continue
        error_entry = []
        for entry in analyse(log_file, CONFIG.LOGS_RE):
            if 'ERROR' in entry.head:
                error_entry.append(entry)
        if len(error_entry) > 0:
            error_report.append('log_name: ' + log_file)
            error_report.append('log_modify_time: ' + str(datetime.fromtimestamp(os.path.getmtime(log_file))))
            error_report.append('log_error_content:')
            last_entry = None
            for entry in error_entry:
                if last_entry is not None and last_entry.body == entry.body:
                        continue
                error_report.append(entry.head)
                error_report.append(entry.body)
                last_entry = entry
            error_report.append('-' * 100)
    if len(error_report) > 0:
        for cursor in range(0, len(error_report), 1000):
            send('log_monitor', '\n'.join(error_report[cursor: cursor + 1000]))


if __name__ == '__main__':
    main()
