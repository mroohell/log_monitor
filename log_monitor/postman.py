# encoding: utf-8
from email.header import Header
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import smtplib

from log_monitor import config as CONFIG


def send(title, content, addrs=CONFIG.EMAIL_TOADDRESS):
    smtpaddr = CONFIG.EMAIL_SMTPADDRESS
    smtpport = CONFIG.EMAIL_SMTPPORT
    from_addr = CONFIG.EMAIL_ACCOUNT
    passwd = CONFIG.EMAIL_PASSWORD
    # make email content
    msg = MIMEMultipart()
    msg.attach(MIMEText(content, 'plain', 'utf-8'))
    msg['From'] = '{:s} <{:s}>'.format('auto email', from_addr)
    msg['To'] = '<' + '> <'.join(addrs) + '>'
    msg['Subject'] = Header(title, 'utf-8').encode()
    # send email
    tries = CONFIG.CONNECTION_TRIES
    while True:
        try:
            smtps = smtplib.SMTP_SSL(smtpaddr, smtpport, timeout=5)
            smtps.login(from_addr, passwd)
            smtps.sendmail(from_addr, addrs, msg.as_string())
            smtps.quit()
            break
        except Exception:
            if tries == 0:
                break
            else:
                tries -= 1
