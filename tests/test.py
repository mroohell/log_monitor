import logging

from log_monitor.analyst import analyse


def setup_module(module):
    logging.basicConfig(level=logging.DEBUG)


def test_analyse():
    count = 0
    for et in analyse('tests/test.log', r'[0-9]{4}-[0-9]{2}-[0-9]{2}'):
        count += 1
        logging.debug(et)
    assert count > 0
